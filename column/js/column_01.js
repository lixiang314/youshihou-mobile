var vmodel;



avalon.ready(function() {

    vmodel = avalon.define("page", function(vm) {
        vm.result = {};
        vm.webConfig = TS_GLOBAL;
    });
    getOrderList();
});


$(document).ready(function() {

});


function getOrderList(){
		$.ajax({
			url : TS_GLOBAL.baseUrl + "api/columns/editors",
			dataType : 'json',
			type : "get",
			async : false,
			success : function(data) {
				if (data.returnCode == 0) {
					vmodel.result = data.result;
					$.each(vmodel.result, function(i,val){

						if(val.limitActive == null){
							val.url = "column_02.html?id="+val.id;
						}else{
							val.url = "column_02.html?id="+val.id+"&isLimit=true";
						}
					});
					
				} else {
					alert(data.message);
				}
			}
		});
}
