var vmodel;
var editorId;


avalon.ready(function() {

    vmodel = avalon.define("page", function(vm) {
        vm.result = {};
        vm.webConfig = TS_GLOBAL;
    });
    editorId = request("id");
    isLimit = request("isLimit");
    if(isLimit == undefined){
		isLimit = false;
	}
	if(editorId != undefined){
		getOrderList(editorId);
	}else{
		getOrderList(1);
	}
	// getOrderList();
});


$(document).ready(function() {

});


function getOrderList(id){
		$.ajax({
			url : TS_GLOBAL.baseUrl + "api/columns?editor="+id,
			dataType : 'json',
			type : "get",
			async : false,
			success : function(data) {
				if (data.returnCode == 0) {
					vmodel.result = data.result;
					
				} else {
					alert(data.message);
				}
			}
		});
}
